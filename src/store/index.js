import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import moduleShop from '@/views/app/shop/shopStoreModule'
import moduleProduct from '@/views/app/product-details/productStoreModule'
import moduleCheckout from '@/views/app/checkout/checkoutStoreModule'
import moduleOrder from '@/views/app/order/orderStoreModule'
import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'
import moduleAuth from './auth/moduleAuth'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    appConfig,
    verticalMenu,
    auth: moduleAuth,
    shop: moduleShop,
    product: moduleProduct,
    checkout: moduleCheckout,
    order: moduleOrder,
  },
  strict: process.env.DEV,
})
