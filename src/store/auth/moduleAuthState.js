import { isUserLoggedIn, getAuthenticatedUser, getAccessToken } from '@/services/auth.service'

export default {
  accessToken: getAccessToken(),
  user: getAuthenticatedUser(),
  isUserLoggedIn: isUserLoggedIn(),
}
