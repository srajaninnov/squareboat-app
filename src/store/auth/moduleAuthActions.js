import constants from '@/constants'
import axios from '@axios'
import { logout } from '@/services/auth.service'

export default {
  sendOTP(ctx, authObj) {
    return new Promise((resolve, reject) => {
      axios
        .post(`${constants.apiHost}/auth/send-otp`, authObj)
        .then(response => resolve(response))
        .catch(error => reject(error))
    })
  },
  logout() {
    logout()
    axios.defaults.headers.common.Authorization = ''
  },
}
