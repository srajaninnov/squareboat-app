import state from './moduleAuthState'
import mutations from './moduleAuthMutations'
import actions from './moduleAuthActions'
import getters from './moduleAuthGetters'

export default {
  isRegistered: false,
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
