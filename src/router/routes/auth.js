export default [
  {
    path: '/get-started',
    name: 'auth-get-started',
    component: () => import('@/views/auth/Main.vue'),
    meta: {
      layout: 'full',
      resource: 'Auth',
      redirectIfLoggedIn: true,
    },
  },
]
