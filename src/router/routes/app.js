export default [
  // *===============================================---*
  // *--------- APP ------------------------------------*
  // *===============================================---*
  {
    path: '/',
    redirect: '/shop',
  },
  {
    path: '/shop',
    name: 'app-shop',
    component: () => import('@/views/app/shop/Shop.vue'),
    meta: {
      // contentRenderer: 'sidebar-left-detached',
      contentClass: 'ecommerce-application',
      pageTitle: 'Shop',
      breadcrumb: [
        {
          text: 'Shop',
          to: '/shop',
        },
        {
          text: 'Product List',
          active: true,
        },
      ],
    },
  },
  /* {
    path: '/wishlist',
    name: 'app-wishlist',
    component: () => import('@/views/app/wishlist/Wishlist.vue'),
    meta: {
      pageTitle: 'Wishlist',
      contentClass: 'ecommerce-application',
      breadcrumb: [
        {
          text: 'ECommerce',
        },
        {
          text: 'Wishlist',
          active: true,
        },
      ],
    },
  }, */
  {
    path: '/checkout',
    name: 'app-checkout',
    component: () => import('@/views/app/checkout/Checkout.vue'),
    meta: {
      pageTitle: 'Checkout',
      contentClass: 'ecommerce-application',
      breadcrumb: [
        {
          text: 'Shop',
          to: '/shop',
        },
        {
          text: 'Checkout',
          active: true,
        },
      ],
    },
  },
  {
    path: '/product/:slug',
    name: 'app-product-details',
    component: () => import('@/views/app/product-details/ProductDetails.vue'),
    meta: {
      pageTitle: 'Product Details',
      contentClass: 'ecommerce-application',
      breadcrumb: [
        {
          text: 'Shop',
          to: '/shop',
        },
        {
          text: 'Product Details',
          active: true,
        },
      ],
    },
  },
  {
    path: '/order-confirmation/:orderReferenceId',
    name: 'app-order',
    component: () => import('@/views/app/order/OrderConfirmation.vue'),
    meta: {},
  },
]
