import store from '@/store'
import Vue from 'vue'
import VueRouter from 'vue-router'

import auth from './routes/auth'
import app from './routes/app'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    ...auth,
    ...app,
    {
      path: '/error-404',
      name: 'error-404',
      component: () => import('@/views/error/Error404.vue'),
      meta: {
        layout: 'full',
      },
    },
    {
      path: '*',
      redirect: 'error-404',
    },
  ],
})

router.beforeEach((to, _, next) => {
  const isLoggedIn = store.state.auth.isUserLoggedIn

  if (to.name !== 'auth-get-started') {
    // Redirect to login if not logged in
    if (!isLoggedIn) return next({ name: 'auth-get-started' })
  }

  // Redirect to Shop if logged in
  if (to.meta.redirectIfLoggedIn && isLoggedIn) {
    next({ name: 'app-shop' })
  }

  return next()
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
  if (appLoading) {
    appLoading.style.display = 'none'
  }
})

export default router
