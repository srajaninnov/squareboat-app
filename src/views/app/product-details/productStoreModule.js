import axios from '@axios'
import data from '@/data/products'
import constants from '@/constants'

export default {
  namespaced: true,
  state: {
    data,
    cartItemsCount: (() => {
      const userData = JSON.parse(localStorage.getItem('userData'))
      return userData && userData.extras ? userData.extras.eCommerceCartItemsCount : 0
    })(),
  },
  getters: {},
  mutations: {
    UPDATE_CART_ITEMS_COUNT(state, count) {
      state.cartItemsCount = count
    },
  },
  actions: {
    fetchProduct(ctx, { productId }) {
      return new Promise((resolve, reject) => {
        // Convert Id to number
        const pId = Number(productId)

        const productIndex = data.products.findIndex(p => p.id === pId)
        const product = data.products[productIndex]

        if (product) {
          // Add data of wishlist and cart
          product.isInWishlist = data.userWishlist.findIndex(p => p.productId === product.id) > -1
          product.isInCart = data.userCart.findIndex(p => p.productId === product.id) > -1

          // * Add Dummy data for details page
          product.colorOptions = ['primary', 'success', 'warning', 'danger', 'info']

          resolve({ responseCode: 200, data: product })
        }
        reject(new Error({ responseCode: 404 }))
      })
    },

    // ------------------------------------------------
    // Product Actions
    // ------------------------------------------------

    addProductInCart(ctx, { _userId, _productId, quantity }) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${constants.apiHost}/carts`, { _userId, _productId, quantity })
          .then(response => resolve(response.data))
          .catch(error => reject(error))
      })
    },
    removeProductFromCart(ctx, { _cartItemId }) {
      return new Promise((resolve, reject) => {
        axios
          .delete(`${constants.apiHost}/carts/${_cartItemId}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
