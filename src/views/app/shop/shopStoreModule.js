// import axios from '@axios'
import data from '@/data/products'

import { paginateArray, sortCompare } from './utils'

// const nextDay = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
// const nextWeek = new Date(nextDay.getTime() + 7 * 24 * 60 * 60 * 1000)

export default {
  namespaced: true,
  state: {
    data,
    cartItemsCount: (() => {
      const userData = JSON.parse(localStorage.getItem('userData'))
      return userData && userData.extras ? userData.extras.eCommerceCartItemsCount : 0
    })(),
  },
  getters: {},
  mutations: {
    UPDATE_CART_ITEMS_COUNT(state, count) {
      state.cartItemsCount = count
    },
  },
  actions: {
    fetchProducts(ctx, payload) {
      // eslint-disable-next-line no-unused-vars
      return new Promise((resolve, reject) => {
        const {
          q = '', sortBy = 'featured', perPage = 12, page = 1,
        } = payload

        const queryLowered = q.toLowerCase()

        const filteredData = data.products.filter(product => product.name.toLowerCase().includes(queryLowered))

        let sortDesc = false
        const sortByKey = (() => {
          if (sortBy === 'price-desc') {
            sortDesc = true
            return 'price'
          }
          if (sortBy === 'price-asc') {
            return 'price'
          }
          sortDesc = true
          return 'id'
        })()

        const sortedData = filteredData.sort(sortCompare(sortByKey))
        if (sortDesc) sortedData.reverse()

        const paginatedData = JSON.parse(JSON.stringify(paginateArray(sortedData, perPage, page)))

        paginatedData.forEach(product => {
          /* eslint-disable no-param-reassign */
          product.isInWishlist = data.userWishlist.findIndex(p => p.productId === product.id) > -1
          product.isInCart = data.userCart.findIndex(p => p.productId === product.id) > -1
          /* eslint-enable */
        })

        resolve([
          200,
          {
            products: paginatedData,
            total: filteredData.length,
            userWishlist: data.userWishlist,
            userCart: data.userCart,
          },
        ])
      })
    },
  },
}
