import store from '@/store'
import { useRouter } from '@core/utils/utils'
import { BToast } from 'bootstrap-vue'
import data from '@/data/products'

export const useEcommerce = () => {
  // eslint-disable-next-line arrow-body-style
  const addProductInWishlist = productId => {
    return store.dispatch('app-ecommerce/addProductInWishlist', { productId })
  }

  // eslint-disable-next-line arrow-body-style
  const removeProductFromWishlist = productId => {
    return store.dispatch('app-ecommerce/removeProductFromWishlist', { productId })
  }

  // eslint-disable-next-line arrow-body-style
  const addProductInCart = ({ _userId, _productId, quantity }) => {
    return store.dispatch('product/addProductInCart', { _userId, _productId, quantity })
  }

  // eslint-disable-next-line arrow-body-style
  const removeProductFromCart = ({ _cartItemId }) => {
    return store.dispatch('product/removeProductFromCart', { _cartItemId })
  }

  return {
    addProductInWishlist,
    removeProductFromWishlist,
    addProductInCart,
    removeProductFromCart,
  }
}

export const useEcommerceUi = () => {
  const { router } = useRouter()

  const toggleProductInWishlist = product => {
    const { addProductInWishlist, removeProductFromWishlist } = useEcommerce()
    if (product.isInWishlist) {
      removeProductFromWishlist(product.id).then(() => {
        // eslint-disable-next-line no-param-reassign
        product.isInWishlist = false
      })
    } else {
      addProductInWishlist(product.id).then(() => {
        // eslint-disable-next-line no-param-reassign
        product.isInWishlist = true
      })
    }
  }

  const getProductById = _productId => {
    const pId = Number(_productId)

    const productIndex = data.products.findIndex(p => p.id === pId)
    const product = data.products[productIndex]

    return product
  }

  const handleCartActionClick = (_userId, product, quantity) => {
    const { addProductInCart } = useEcommerce()

    const i = store.state.checkout.cartItems.findIndex(item => item._productId === product.id)

    const bToast = new BToast()
    const productTitle = getProductById(product.id).name

    if (i !== -1) {
      router.push({ name: 'app-checkout' })
    } else {
      addProductInCart({ _userId, _productId: product.id, quantity }).then(res => {
        // eslint-disable-next-line no-param-reassign
        // Update cart items count
        // store.commit('app-ecommerce/UPDATE_CART_ITEMS_COUNT', store.state['app-ecommerce'].cartItemsCount + 1)
        store.commit('checkout/ADD_CART_ITEM', res.data)

        bToast.$bvToast.toast(productTitle, {
          title: 'Item Added to Cart',
          variant: 'success',
          solid: true,
        })
      }).catch(err => {
        bToast.$bvToast.toast(productTitle, {
          title: 'Error Adding Item to Cart',
          variant: 'danger',
          solid: true,
        })
        throw err
      })
    }
  }

  const handleWishlistCartActionClick = (product, removeProductFromWishlistUi) => {
    const { addProductInCart, removeProductFromWishlist } = useEcommerce()

    if (product.isInCart) {
      router.push({ name: 'apps-e-commerce-checkout' })
    } else {
      addProductInCart(product.id)
        .then(() => {
          // eslint-disable-next-line no-param-reassign
          product.isInCart = true
          removeProductFromWishlist(product.id)

          // Update cart items count
          store.commit('app-ecommerce/UPDATE_CART_ITEMS_COUNT', store.state['app-ecommerce'].cartItemsCount + 1)
        })
        .then(() => {
          // eslint-disable-next-line no-param-reassign
          product.isInWishlist = false
          removeProductFromWishlistUi(product)
        })
    }
  }
  return {
    toggleProductInWishlist,
    handleCartActionClick,
    handleWishlistCartActionClick,
  }
}
