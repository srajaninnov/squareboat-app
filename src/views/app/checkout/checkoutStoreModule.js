import axios from '@axios'
import products from '@/data/products'
import constants from '@/constants'

export default {
  namespaced: true,
  state: {
    data: products,
    cartItemsCount: (() => {
      const userData = JSON.parse(localStorage.getItem('userData'))
      return userData && userData.extras ? userData.extras.eCommerceCartItemsCount : 0
    })(),
    cartItems: [],
  },
  getters: {
    totalAmount: state => () => {
      let totalAmount = 0
      state.cartItems.forEach(item => {
        products.products.forEach(product => {
          if (product.id === Number(item._productId)) totalAmount += Number(product.price)
        })
      })
      return totalAmount
    },
  },
  mutations: {
    ADD_CART_ITEM(state, value) {
      state.cartItems.push(value)
    },
    UPDATE_CART_ITEMS_COUNT(state, count) {
      state.cartItemsCount = count
    },
    SET_CART_ITEMS(state, value) {
      state.cartItems = value
    },
    DELETE_CART_ITEM(state, value) {
      const index = state.cartItems.findIndex(item => item._id === value._id)
      if (index > -1) {
        state.cartItems.splice(index, 1)
      }
    },
    FLUSH_CART(state) {
      state.cartItems = []
    },
  },
  actions: {
    fetchCartProducts({ commit }, { _userId }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`${constants.apiHost}/carts/users/${_userId}`)
          .then(response => {
            commit('SET_CART_ITEMS', response.data.data)
            resolve(response)
          })
          .catch(error => reject(error))
      })
    },
  },
}
