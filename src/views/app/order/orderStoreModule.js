import axios from '@axios'
import products from '@/data/products'
import constants from '@/constants'

export default {
  namespaced: true,
  state: {
    data: products,
    cartItemsCount: (() => {
      const userData = JSON.parse(localStorage.getItem('userData'))
      return userData && userData.extras ? userData.extras.eCommerceCartItemsCount : 0
    })(),
  },
  getters: {},
  mutations: {
    UPDATE_CART_ITEMS_COUNT(state, count) {
      state.cartItemsCount = count
    },
  },
  actions: {
    createOrder(ctx, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${constants.apiHost}/orders`, payload)
          .then(response => resolve(response.data))
          .catch(error => reject(error))
      })
    },
  },
}
