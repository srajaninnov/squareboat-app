import useJwt from '@core/auth/jwt/useJwt'
import axios from '@axios'
import jwtOTPConfig from './jwtOTPConfig'

const { jwt } = useJwt(axios, jwtOTPConfig)
export default jwt
