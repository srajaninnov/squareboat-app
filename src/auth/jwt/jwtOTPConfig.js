import constants from '@/constants'
import defaultConfig from '@core/auth/jwt/jwtDefaultConfig'

defaultConfig.sendOTP = `${constants.apiHost}/auth/send-otp`
defaultConfig.loginEndpoint = `${constants.apiHost}/auth/verify-otp`
defaultConfig.storageTokenKeyName = 'accessToken'

export default defaultConfig
