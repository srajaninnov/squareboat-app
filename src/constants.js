const constants = {
  apiHost: 'http://18.116.238.120:3030/api',
  domain: '18.116.238.120:3030',
  cookieScope: '.http://18.116.238.120:8080',
  url: 'http://18.116.238.120:8080',
}

if (process.env.NODE_ENV === 'development') {
  constants.apiHost = 'http://localhost:3030/api'
  constants.domain = 'localhost'
  constants.cookieScope = 'localhost'
  constants.url = 'http://localhost:8080'
}

export default constants
