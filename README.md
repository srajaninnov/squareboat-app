# APP Setup Instructions

## Initial Requirements
* Node: v14.17.0
* Npm: 7.7.5

Please make sure, you have all of the above installed.

## Steps for Code Setup
1. npm install
2. npm run serve

# Pages
## Login or Signup
### [http://localhost:8080/get-started](http:localhost:8080/get-started)

## Shop
### [http://localhost:8080/shop](http:localhost:8080/shop)

## Product Detail
### [http://localhost:8080/product/:slug](http:localhost:8080/product/apple-mac-book-air-latest-model-13-3-display-silver-23)

## Checkout and Make Payment
### [http://localhost:8080/checkout](http:localhost:8080/checkout)

## Other Pages
* Order Confirmation: [http://localhost:8080/order-confirmation/1234567890](http://localhost:8080/order-confirmation/1234567890)
* Error 404: [http://localhost:8080/error-404](http://localhost:8080/error-404)


# Credits
* Images: Freepik
* Ecommerce Data: Google & other sources
* Branding: Squareboat Logo
