# build stage
FROM node:14 as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install -f
COPY ./ .
RUN npm run build

# production stage
FROM nginx as production-stage
RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf